<?php
session_start();
require_once("../../vendor/autoload.php");
use App\Delivery_master;
$object= new Delivery_master();
$object->setData($_POST);
$object->store();

use App\Delivery_details;
$details = new Delivery_details();
$details->setData($_SESSION)->store();
?>
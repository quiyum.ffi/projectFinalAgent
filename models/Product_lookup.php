<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 2/22/2017
 * Time: 4:23 PM
 */

namespace App;
use App\database\Database;
use App\Message\Message;
use App\Utility\Utility;
use PDO;

class Product_lookup extends Database
{
    public $id;
    public $product_name;
    public $unit_price;

    public function setData($allPostData=null){

        if(array_key_exists("id",$allPostData)){
            $this->id= $allPostData['id'];
        }

        if(array_key_exists("product_name",$allPostData)){
            $this->product_name= $allPostData['product_name'];
        }
        if(array_key_exists("unit_price",$allPostData)){
            $this->unit_price= $allPostData['unit_price'];
        }

    }

    public function getUnitprice($ids){
        
        $sql = "SELECT `unit_price` FROM `product_lookup` WHERE id IN($ids)";
       
        $STH = $this->DBH->query($sql);
        
        $STH->setFetchMode(PDO::FETCH_ASSOC);
       // $STH->bindParam(1,$ids);
        $STH->execute();
        while ($row=$STH->fetch()){
            $alldata[]=$row['unit_price'];

        }
        return $alldata;


    }
    public function showalldraft($ids){

        $sql = "SELECT product_name FROM `product_lookup` WHERE id IN($ids)";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $STH->execute();
        while ($row=$STH->fetch()){
            $alldata[]=$row['product_name'];

        }
        return $alldata;



    }

    public function store(){
        $arrData  =  array($this->product_name,$this->unit_price);

        $query= 'INSERT INTO product_lookup (product_name,unit_price) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
           // die();
            Message::setMessage("Success! Product details added successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }

        Utility::redirect('../../views/manager/addProduct.php');

    }
    public function storeadmin(){
        $arrData  =  array($this->product_name,$this->unit_price);

        $query= 'INSERT INTO product_lookup (product_name,unit_price) VALUES (?,?)';

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            // die();
            Message::setMessage("Success! Product details added successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been stored!");
        }

        Utility::redirect('../../views/admin/addProduct.php');

    }
    public function showall(){

        $sql = "Select * from product_lookup where status='available'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function view(){

        $sql = "Select * from product_lookup where id=".$this->id;
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetch();
    }
    public function update(){
        $arrData  =  array($this->product_name,$this->unit_price);

        $query= 'UPDATE product_lookup SET product_name = ?, unit_price= ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been updated!");
        }

        Utility::redirect('../../views/manager/productList.php');

    }
    public function trash(){
        $arrData  =  array("not available");

        $query= 'UPDATE product_lookup SET status = ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been successfully Trashed!");
        }
        else{
            Message::setMessage("Failed! Data has not been Trashed!");
        }

        Utility::redirect('../../views/manager/productList.php');

    }
    public function trashed(){

        $sql = "Select * from product_lookup where status='not available'";
        $STH = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        return $STH->fetchAll();
    }
    public function recover(){
        $arrData  =  array("available");

        $query= 'UPDATE product_lookup SET status = ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been successfully Recovered!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!");
        }

        Utility::redirect('../../views/manager/productList.php');

    }
    public function delete(){
        $sql = "DELETE from product_lookup WHERE id=".$this->id;

        $result= $this->DBH->exec($sql);


        if($result){
            Message::setMessage("Success! Data has been Deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Deleted!");
        }
        Utility::redirect('../../views/manager/productList.php');
    }

    public function updateadmin(){
        $arrData  =  array($this->product_name,$this->unit_price);

        $query= 'UPDATE product_lookup SET product_name = ?, unit_price= ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been updated successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been updated!");
        }

        Utility::redirect('../../views/admin/productList.php');

    }
    public function trashadmin(){
        $arrData  =  array("not available");

        $query= 'UPDATE product_lookup SET status = ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been successfully Trashed!");
        }
        else{
            Message::setMessage("Failed! Data has not been Trashed!");
        }

        Utility::redirect('../../views/admin/productList.php');

    }
    public function recoveradmin(){
        $arrData  =  array("available");

        $query= 'UPDATE product_lookup SET status = ? WHERE id='.$this->id;

        $STH = $this->DBH->prepare($query);
        $result = $STH->execute($arrData);

        if($result){
            Message::setMessage("Success! Data has been successfully Recovered!");
        }
        else{
            Message::setMessage("Failed! Data has not been Recovered!");
        }

        Utility::redirect('../../views/admin/productList.php');

    }
    public function deleteadmin(){
        $sql = "DELETE from product_lookup WHERE id=".$this->id;

        $result= $this->DBH->exec($sql);


        if($result){
            Message::setMessage("Success! Data has been Deleted successfully!");
        }
        else{
            Message::setMessage("Failed! Data has not been Deleted!");
        }
        Utility::redirect('../../views/admin/productList.php');
    }

}

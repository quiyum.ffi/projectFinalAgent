<!-- jQuery -->
<script src="<?php echo base_url; ?>resources/bower_components/jquery/dist/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url; ?>resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="<?php echo base_url; ?>resources/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Morris Charts JavaScript -->
<script src="<?php echo base_url; ?>resources/bower_components/raphael/raphael-min.js"></script>
<!--<script src="<?php /*echo base_url; */?>resources/bower_components/morrisjs/morris.min.js"></script>-->
<!--<script src="<?php /*echo base_url; */?>resources/js/morris-data.js"></script>-->
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url; ?>resources/dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_url; ?>resources/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (1550);
        $('#message').fadeIn (1550);
        $('#message').fadeOut (50);

    })
</script>
